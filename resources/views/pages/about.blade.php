@extends('main')

@section('title', 'Reach Legal - About')

@section('content')

    <section class="about_1">
        <h2>ABOUT US</h2>
    </section>
    <div class="about_title">
        <p>Who We Are</p>
        <div class="about_title_line"></div>
    </div>
    <section class="about_2">
        <h6>Lorem ipsum dolor sit amet</h6>
        <div class="about_2_text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquet
                metus non lectus porttitor, ac hendrerit odio lacinia. Cras quis libero vel
                tortor porta suscipit ut in urna. Vestibulum ante ipsum primis in faucibus
                orci luctus et ultrices posuere cubilia Curae; Integer id interdum dolor.
                Suspendisse ac consectetur eros, sit amet eleifend libero. Mauris nec nulla
                sodales dolor blandit eleifend. Aliquam et libero accumsan, interdum est ac,
                aliquet mauris. Praesent eu metus vitae magna semper cursus. Curabitur sodales
                consectetur urna. Suspendisse feugiat
                tincidunt sapien at tincidunt. Maecenas a magna urna. Sed a interdum orci.
                Vestibulum arcu elit, faucibus quis lorem at, posuere sodales mi. Nam eget
                purus et justo vestibulum scelerisque eu auctor nisl. Aliquam finibus diam
                vel ex pellentesque, vitae tempus lectus rhoncus. Sed eget </p>
        </div>
    </section>
    <div class="about_title">
        <p>What We Do</p>
        <div class="about_title_line"></div>
    </div>
    <section class="about_3">
        <div class="about_3_size">
            <div class="about_3_left">
                <img src="{{asset('assets/images/general/about1.png')}}" alt="">
            </div>
            <div class="about_3_right">
                <h6>Lorem ipsum dolor sit amet</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquet
                    metus non lectus porttitor, ac hendrerit odio lacinia. Cras quis libero
                    vel tortor porta suscipit ut in urna. Vestibulum ante ipsum primis in
                    faucibus orci luctus et ultrices posuere cubilia Curae; Integer id interdum dolor.
                    Suspendisse ac consectetur eros, sit amet eleifend libero. Mauris nec
                    nulla sodales dolor blandit eleifend. Aliquam et libero accumsan,
                    interdum est ac, aliquet mauris. Praesent eu metus vitae magna semper cursus.
                    Curabitur sodales consectetur urna. </p>
            </div>
        </div>
    </section>
    <section class="about_4">
        <h3>Our team</h3>
        <p>Our team how people get Legal help Vestibulum ante ipsum primis in faucibus
            orci luctus et ultrices posuere cubilia Curae; Integer id interdum dolor.
            Suspendisse ac consectetur eros, sit amet eleifend libero. Mauris nec nulla
            sodales dolor blandit eleifend. Aliquam et libero accumsan, interdum est ac,
            aliquet mauris. Praesent eu metus vitae magna semper cursus. Curabitur sodales consectetur urna. </p>
        <button type="button" name="button">Join Now </button>
    </section>
    <section class="lawyers_7">
        <div class="opacity_bg">
        </div>
    </section>

@endsection
